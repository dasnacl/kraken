open OUnit2
open Lib.Ast
open Lib

let parse txt p = Parsco.run p txt

let test_number_0 _ =
  begin
  let res = parse "0" Parsco.number in
  match res with
  | Ok x -> assert_equal x "0"
  | _ -> assert_failure "Parsing 0 fails"
  end

let test_number_7 _ =
  begin
  let res = parse "7" Parsco.number in
  match res with
  | Ok x -> assert_equal x "7"
  | _ -> assert_failure "Parsing 7 fails"
  end

let test_number_1729 _ =
  begin
  let open Parsco in
  let res = parse "    1729   "  (ws *> number <* ws) in
  match res with
  | Ok x -> assert_equal x "1729"
  | _ -> assert_failure "Parsing 1729 with surrounding spaces fails"
  end

let test_no_leading_zero_0 _ =
  begin
  let res = parse "00" Parsco.number in
  match res with
  | Ok _ -> assert_failure "Parsing 00 should fail"
  | _ -> ()
  end

let test_no_leading_zero_5 _ =
  begin
  let res = parse "05" Parsco.number in
  match res with
  | Ok _ -> assert_failure "Parsing with leading zero should fail"
  | _ -> ()
  end

let test_no_leading_zero_3445 _ =
  begin
  let res = parse "03445" Parsco.number in
  match res with
  | Ok _ -> assert_failure "Parsing with leading zero should fail"
  | _ -> ()
  end

let test_no_leading_zeros _ =
  begin
  let res = parse "00003445" Parsco.number in
  match res with
  | Ok _ -> assert_failure "Parsing with leading zero should fail"
  | _ -> ()
  end

let test_simple_expression_leftassoc _ =
  begin
  let res = parse "0+1+2" (Parse.expr 0) in
  match res with
  | Ok e -> begin
    match !e with
    | Kraken.Op_e(e1,Kraken.Plus,_) ->
      begin
        match !e1 with
        | Kraken.Op_e(_,Kraken.Plus,_) -> ()
        | _ -> assert_failure "Parsing addition is left associative"
      end
    | _ -> assert_failure "Parsing addition is left associative"
    end
  | _ -> assert_failure "Parsing addition is left associative"
  end

let test_simple_expression_right_paren _ =
  begin
  let res = parse "0-(1-2)" (Parse.expr 0) in
  match res with
  | Ok e -> begin
    match !e with
    | Kraken.Op_e(_,Kraken.Minus,e2) ->
      begin
        match !e2 with
        | Kraken.Op_e(_,Kraken.Minus,_) -> ()
        | _ -> assert_failure "Parsing subtraction is left associative"
      end
    | _ -> assert_failure "Parsing subtraction is left associative"
    end
  | _ -> assert_failure "Parsing subtraction is left associative"
  end

let test_simple_expression_parens_and_spaces _ =
  begin
  let res = parse "  ( (0 ) ) -( 1    -   2 )" (Parse.expr 0) in
  match res with
  | Ok _ -> ()
  | _ -> assert_failure "Parsing a simple expression ignores spaces and handles parentheses"
  end
