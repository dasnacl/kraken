
open OUnit2


let suite =
  "Parser::expressions" >::: [
    "test_number_0" >:: Parser.test_number_0;
    "test_number_7" >:: Parser.test_number_7;
    "test_number_1729" >:: Parser.test_number_1729;
    "test_no_leading_zero_0" >:: Parser.test_no_leading_zero_0;
    "test_no_leading_zero_5" >:: Parser.test_no_leading_zero_5;
    "test_no_leading_zero_3445" >:: Parser.test_no_leading_zero_3445;
    "test_no_leading_zeros" >:: Parser.test_no_leading_zeros;
    "test_simple_expressions_leftassoc" >:: Parser.test_simple_expression_leftassoc;
    "test_simple_expression_right_paren" >:: Parser.test_simple_expression_right_paren;
    "test_simple_expression_parens_and_spaces" >:: Parser.test_simple_expression_parens_and_spaces;
  ]

let () =
  run_test_tt_main suite
