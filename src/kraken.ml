open Lib

let print_list = List.iter (fun a -> let _ = Ast.print_stmt a in Printf.printf "\n")

let maybe_read_line () =
  try Some(read_line())
  with End_of_file -> None

let rec input_loop acc =
  match maybe_read_line () with
  | Some(line) -> input_loop (acc @ [line])
  | None -> String.concat "\n" acc

let run_program input =
  let parse_result = Parse.parse_text input in
  match parse_result with
  | Ok prog ->
    begin
      Printf.printf "\nInput\n-----\n";
      print_list prog;
      Elab.typecheck prog |> List.iter (fun x -> match x with
                                                 | Ok _ -> ()
                                                 | Error s -> Printf.printf "%s\n" s);
      Printf.printf "\nOutput\n-------\n";
      print_list (Eval.run prog)
    end
  | Error err -> Printf.printf "%d: error: %s" err.pos err.info


let main () =
  if Array.length Sys.argv = 1 then
    let input_str = input_loop [] in
    run_program input_str
  else
    (*List.iter (fun file -> run_file file) (List.tl (Array.to_list Sys.argv))*) ()

let _ = main ()
