open Batteries

module UnionFind = struct
  type t = { parent : int BatDynArray.t ; rank : int BatDynArray.t }

  let make n = { parent = BatDynArray.make n ; rank = BatDynArray.make n }
  let size uf = BatDynArray.length uf.rank
  let rec find uf i =
    let pi = BatDynArray.get uf.parent i in
    if pi == i then
      i
    else begin
      let ci = find uf pi in
      BatDynArray.set uf.parent i ci;
      ci
    end
  let union ({parent = p; rank = r} as uf) x y =
    let cx = find uf x in
    let cy = find uf y in
    if cx != cy then begin
      if BatDynArray.get r cx > BatDynArray.get r cy then
        begin BatDynArray.set p cy cx; cx end
      else if BatDynArray.get r cx < BatDynArray.get r cy then
        begin BatDynArray.set p cx cy; cy end
      else begin
        BatDynArray.set r cx ((BatDynArray.get r cx) + 1);
        BatDynArray.set p cy cx; BatDynArray.get r cx
      end
    end
    else
      undefined "x and y are not of different equivalence classes"
  (** Adds a new equivalence class *)
  let push uf = BatDynArray.add uf.rank (BatDynArray.length uf.rank);
                BatDynArray.add uf.parent (BatDynArray.length uf.parent);
                (BatDynArray.length uf.rank) - 1
end

module type EggTerm = sig
  type t
  val compare : t -> t -> int
  val children : (t, (int BatDynArray.t)) Hashtbl.t
  val hash : t -> int
end

module Egg(Term:EggTerm) = struct
  module Termset = Set.Make(Term)
  type term = Term.t

  (** Defines a rewrite rule *)
  type rule = Eq of term * term

  (** An equivalence class is a set of nodes. In order to speedup congruence closure, we store parents *)
  type eclass = { terms : term BatDynArray.t ; parents : (term*int) BatDynArray.t }

  (** An egraph consists of (1) a map from terms to eq.class id's, (2) eq.class ids to actual eq. classes, and (3) a union find on eq class ids *)
  type egraph = { term_to_class : (term,int) Hashtbl.t ;
                  class_to_equi : (int, eclass) Hashtbl.t ;
                  ufind_equis   : UnionFind.t ;
                  dirty_unions  : int BatDynArray.t
                }

  let initial_egraph (_ : unit) : egraph = {
    term_to_class = Hashtbl.create 497;
    class_to_equi = Hashtbl.create 503;
    ufind_equis   = UnionFind.make 503;
    dirty_unions  = BatDynArray.make 123;
  }
  let find_root (e : egraph) (id : int) = UnionFind.find e.ufind_equis id
  let canonicalize (e : egraph) t = let _ = Hashtbl.find Term.children t |> BatDynArray.map (fun a -> find_root e a) in ()
  let in_same_class (e : egraph) (t1 : term) (t2 : term) =
    let a = Hashtbl.find_option e.term_to_class t1 in
    let b = Hashtbl.find_option e.term_to_class t2 in
    a = b
  let find_class (e : egraph) (t : term) =
    canonicalize e t;
    match Hashtbl.find_option e.term_to_class t with
    | None -> None
    | Some id -> Some (find_root e id)

  let insert (e : egraph) (t : term) =
    match find_class e t with
    | None -> begin
        let next = UnionFind.push e.ufind_equis in
        let cls = {
          terms = BatDynArray.singleton t;
          parents = BatDynArray.make 43
        } in
        Hashtbl.find Term.children t |> BatDynArray.iter (fun child ->
            BatDynArray.add (Hashtbl.find e.class_to_equi child).parents (t,next)
        );
        Hashtbl.add e.class_to_equi next cls;
        Hashtbl.add e.term_to_class t next;
        next
      end
    | Some c -> c

  let union (e : egraph) i1 i2 =
    let id1 = find_root e i1 in
    let id2 = find_root e i2 in
    if id1 != id2 then
    begin
      let id3 = UnionFind.union e.ufind_equis id1 id2 in
      let (id_to, id_from) = if id3 == id1 then
                               (id1, id2)
                             else if id3 == id2 then
                               (id2, id1)
                             else
                               undefined "unreachable code path" in
      BatDynArray.add e.dirty_unions id3;
      let from_class = Hashtbl.find e.class_to_equi id_from in
      let to_class   = Hashtbl.find e.class_to_equi id_to in
      (* empty out from and put all in to *)
      BatDynArray.append from_class.terms to_class.terms;
      BatDynArray.clear from_class.terms;
      (* recanonicalize *)
      (Hashtbl.find e.class_to_equi id_to).terms |> BatDynArray.iter (fun t ->
        Hashtbl.remove e.term_to_class t;
        canonicalize e t;
        Hashtbl.add e.term_to_class t id_to
      );
      (* merge parents *)
      BatDynArray.iter (fun (p,id) -> BatDynArray.add to_class.parents (p,find_root e id)) from_class.parents;
      (* destroy from eq.class *)
      Hashtbl.remove e.class_to_equi id_from;
      ()
    end else ()

  let repair (e : egraph) id =
    let cls = Hashtbl.find e.class_to_equi id in
    cls.parents |> BatDynArray.iter (fun (t,t_id) ->
      Hashtbl.remove e.term_to_class t;
      canonicalize e t;
      Hashtbl.add e.term_to_class t (find_root e t_id)
    );
    let new_parents = Hashtbl.create 1729 in
    cls.parents |> BatDynArray.iter (fun (t,t_id) ->
      canonicalize e t;
      begin
      match Hashtbl.find_option new_parents t with
      | None -> ()
      | Some np -> union e t_id np
      end;
      Hashtbl.modify t (fun _ -> find_root e t_id) new_parents
    );
    let x = (Hashtbl.find e.class_to_equi id).parents in
    BatDynArray.clear x;
    BatDynArray.append (Hashtbl.to_list new_parents |> BatDynArray.of_list) x

  let rec rebuild (e : egraph) =
    if not (BatDynArray.empty e.dirty_unions) then
    begin
        let todo = e.dirty_unions |> BatDynArray.to_list |> List.map (fun id -> find_root e id) |> Set.of_list in
        BatDynArray.clear e.dirty_unions;
        todo |> Set.iter (fun id -> repair e id);
        rebuild e
    end
    else
      ()

  let ematch (_e : egraph) (_p : term) =
    () (* TODO: match against a pattern *)



end
