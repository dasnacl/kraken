open Ast
open Parsco


type associativity = Leftassoc | Rightassoc
type krop = Krop of Kraken.op | FunctionApplication | FunctionArrow
type op = { kop : krop; text : string; prec : int ; assoc : associativity}

(* The order in the following list is important.
 * Items that are prefixes of others must appear after the superstring! *)

let ops = [{ kop = Krop(Kraken.Plus); text = "+" ; prec = 4 ; assoc = Leftassoc } ;
           { kop = FunctionArrow; text = "->"; prec = 1; assoc = Rightassoc } ;
           { kop = Krop(Kraken.Minus); text = "-"; prec = 4 ; assoc = Leftassoc} ;
           { kop = Krop(Kraken.Mult); text = "*"; prec = 6 ; assoc = Leftassoc } ;
           { kop = Krop(Kraken.Div); text = "/"; prec = 6 ; assoc = Leftassoc } ;
           { kop = Krop(Kraken.LeEq); text = "<="; prec = 3 ; assoc = Leftassoc } ;
           { kop = Krop(Kraken.Le); text = "<"; prec = 3 ; assoc = Leftassoc } ;
           { kop = Krop(Kraken.Eq); text = "="; prec = 2 ; assoc = Leftassoc } ;
           { kop = Krop(Kraken.Ne); text = "!="; prec = 2 ; assoc = Leftassoc } ;
           { kop = Krop(Kraken.GtEq); text = ">="; prec = 3 ; assoc = Leftassoc } ;
           { kop = Krop(Kraken.Gt); text = ">"; prec = 3 ; assoc = Leftassoc } ;
           { kop = FunctionApplication; text = "#app_token"; prec = 9999 ; assoc = Leftassoc } ;
          ]
let fn_app_op = List.find (fun a -> a.text = "#app_token") ops
let handle_assoc o = if o.assoc = Leftassoc then o.prec + 1 else o.prec

let op_table = (ops |> List.map (fun o -> (o.text, o)) |> List.to_seq) |> Hashtbl.of_seq
let op_parsers = ops |> List.map (fun o -> expect o.text)

(* Expressions *)
(** Parses an infix operator symbol. Symbol is taken from the Parse.op table *)
let parse_op = info "Expected a binary operator."
               (Parsco.choice op_parsers) |> Parsco.map (fun x -> Hashtbl.find op_table x)

let parse_type = expect "Type" |> Parsco.map (fun _ -> Kraken.type_e)

let identifier_no_typannot = info "Identifier expected!"
                            (Parsco.many1 (any_char ("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_"
                                                   ^ "αβχδεφγψιθκλμνοπϕρστωξυζϱϑςυ")))
                            |> Parsco.map (fun x -> x |> List.to_seq |> String.of_seq)

(** Parses an identifier *)
let identifier expr = {
  run = fun text ->
    let id_parser = identifier_no_typannot in
    let potential_annot = opt (ws *> expect_prefix ":" ":=" <* ws) in
    let actual_annot = expr 0 |> info "Type annotation expected." in
    let process_annot (i,a) = match a with
      | Some _ -> wrap i <*> actual_annot |> bind (fun (e,t) -> Kraken.var_t e t |> wrap)
      | None -> Kraken.var_e i |> wrap
    in
    match ((id_parser <*> potential_annot) |> bind process_annot).run text with
    | text', Ok e -> text', Ok e
    | text', Error s -> text', Error s
}
(** Parses an integer literal that might have a - as prefix. *)
let literal =
  let minus = opt (expect "-") in
  let num = Parsco.map (fun x -> int_of_string x) Parsco.number in
  (minus <*> num) |> map (fun (minus, num) ->
      begin
        match minus with
        | None -> Kraken.int_e num
        | Some _ -> Kraken.int_e (-num)
      end
    )

(* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! *)
(* For all prefix expressions that recurse into expr:                   *)
(* we need to unfold once, it loops infinitely otherwise when building  *)
(* the parser generator. The parser itself does not loop infinitely ;^) *)

(** Parses a list of parameters of some function, e.g. `(a b c : Type)` or `a` *)
let parse_params expr : Kraken.exp list parser = {
  run = fun text ->
    let typ_annot = expr 0 |> info "Type annotation expected." in
    let potential_annot = opt (ws *> expect ":" <* ws) in
    let process_annot (i,a) = match a with
      | Some _ -> wrap i <*> typ_annot
                  |> bind (fun (e,t) -> List.fold_right (fun elem acc -> (Kraken.var_t elem t) :: acc) e [] |> wrap)
      | None -> List.fold_right (fun elem acc -> (Kraken.var_e elem) :: acc) i [] |> wrap
    in
    let param_block_inner = (many1 (ws *> identifier_no_typannot)) <*> potential_annot |> bind process_annot in
    let parenthesized_param_block = expect "(" *> param_block_inner <* expect ")" in
    let param_block = param_block_inner <|> parenthesized_param_block in
    (many (ws *> param_block <* ws) |> map List.concat).run text
}
let parenthesized_expr expr = {
  run = fun text ->
    match (expect "(").run text with
    | text', Ok _ -> (expr 0 <* expect ")").run text'
    | text', Error s -> text', Error s
}
let parse_let expr = {
  run = fun text ->
    match (expect "let " *> ws *> identifier expr).run text with
    | text', Ok id -> ((ws *> expect ":=" *> expr 0 <* expect ";") <*>
                    expr 0
                    |> map (fun (init,e) -> Kraken.let_e id init e)).run text'
    | text', Error s -> text', Error s
}
let parse_app expr = {
  run = fun text ->
    match (expr 0 <*> expr 0 |> map (fun (a,b) -> Kraken.app_e a b)).run text with
    | text', Ok e -> text', Ok e
    | text', Error s -> text', Error s
}
let parse_lam expr = {
  run = fun text ->
    match ((anys ["\\" ; "λ" ; "∀" ; "forall"]) *> (ws *> (parse_params expr) <* ws <* expect ".") <*> expr 0
          |> map (fun (xs,e) -> List.fold_right (fun id acc -> Kraken.lam_e id acc) xs e)).run text with
    | text', Ok e -> text', Ok e
    | text', Error s -> text', Error s
}

let rec expr (prec : int) : Kraken.exp parser =
  let prefix = (ws *>
    Parsco.choice [
         literal ;
         parse_type ;
         parse_let expr ;
         parse_lam expr ;
         parenthesized_expr expr ;
         identifier expr ; (* <- needs to be last to not match keywords, e.g. `let` *)
       ] <* ws)
  in
  let rec parse_infix (pref : Kraken.exp) : Kraken.exp parser = {
    run = fun text ->
    let krakenop_of_krop o x = wrap (match o with
      | Krop o' -> Kraken.op_e pref o' x
      | FunctionApplication -> Kraken.app_e pref x
      | FunctionArrow -> Kraken.arrow_e pref x)
    in
    let bundle_rhs o rhs = rhs |> bind (krakenop_of_krop o) in
    let recurse_and_bundle o = (ws *> expr (handle_assoc o) <* ws) |> bundle_rhs o.kop in
    match no_end.run text with
    | _, Error _ -> text, Ok pref (* <- it's ok to have no infix, bail out then *)
    | _, _ -> begin
        match (ws *> parse_op <* ws).run text with
        | text_without_op, Ok o ->
          if o.prec < prec then
            text, Ok pref (* This fail means that there is still some up that we need to parse
                           * in a higher call-stack position. That's why we use text here *)
          else
            (info "Expected operand." (recurse_and_bundle o |> Parsco.bind parse_infix)).run text_without_op
        | text'', Error err ->
          begin
          match (anys [")" ; ";" ; "\\" ; "->" ; ":"]).run text'' with
          | _, Ok _ -> text, Ok pref (* We've met a character that delimits an expression*)
          | _, _ ->
            begin
              (* test if we have a function application and recurse accordingly *)
              match prefix.run text'' with
              | _, Ok _ ->
                begin
                  if fn_app_op.prec < prec then
                    text, Ok pref
                  else
                    (recurse_and_bundle fn_app_op |> Parsco.bind parse_infix).run text''
                end
              | _, Error _ -> text'', Error err
            end
          end
        end
  }
  in prefix |> Parsco.bind parse_infix


(* Statements *)
let expr_stmt =
  Parsco.map (fun x -> Kraken.Expr_s x) (expr 0 <* expect ";")

let stmt = expr_stmt

let entry = Parsco.many1 stmt

let read_file (path : string) =
  let ch = open_in path in
  let n = in_channel_length ch in
  let s = really_input_string ch n in
  close_in ch;
  s

let parse_file (path : string) =
  Parsco.run entry (read_file path)

let parse_text (str : string) =
  Parsco.run entry str
