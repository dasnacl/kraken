
(** Applies given binary function elementwise. `list_reduce (+) [1;2;3]` becomes ((1 + 2) + 3) *)
let list_reduce (f : 'a -> 'a -> 'a) (xs : 'a list) : 'a =
  let rec loop res ys =
    match ys with
    | [] -> res
    | y :: ys' -> loop (f res y) ys'
  in
  match xs with
  | x :: y :: xs' -> loop (f x y) xs'
  | _ -> raise (Invalid_argument "List with at least 2 elements expected!")


(** Prints a list seperated by `sep`. `string_of_sep_list string_of_int ", " [1 ; 2 ; 3]` becomes "1, 2, 3" *)
let rec string_of_sep_list string_of_elem (sep : string) = function
  | [] -> ""
  | [x] -> string_of_elem x
  | x :: xs -> begin
      Printf.sprintf "%s%s%s" (string_of_elem x) sep (string_of_sep_list string_of_elem sep xs)
      end
