type position = int
type text = { data : string ; pos : position }

let mk_text (s : string) = { data = s; pos = 0 }
let splice (start : int) (len : int) (s : text) = {
  data = String.sub s.data start len;
  pos = s.pos + start
}

type error = { info : string; pos : int }
type 'a parser = { run : text -> text * ('a, string) result }

(* Define the basis of parser combinators *)

(** A parser that always fails *)
let fail (info : string) = { run = fun x -> x, Error info }
(** A parser that always succeeds, it's basically λx.x *)
let wrap (x : 'a) = { run = fun y -> y,  Ok x }
(** Fails if given input is empty *)
let no_end = { run = fun text ->
  if String.length text.data = 0 then
    text, Error "Unexpected end of input."
  else
    text, Ok ()
}
(** Attach a new failure message for a given parser. *)
let info (info : string) (p : 'a parser) = {
  run = fun text ->
    match p.run text with
    | text', Ok x -> text', Ok x
    | text', Error _ -> text', Error info
}
(** Usual monadic bind. Passes the result of a parser to a function and gives another parser back *)
let bind (f : 'a -> 'b parser) (p : 'a parser) : 'b parser = {
  run = fun text ->
    match p.run text with
    | text', Ok x -> (f x).run text'
    | text', Error err -> text', Error err
}
(** Apply a wrapped function to a wrapped value *)
let applyP (fP : ('a -> 'b) parser) (p1 : 'a parser) : 'b parser =
  fP |> bind (fun f -> p1 |> bind (fun x -> wrap (f x)))
(** Lift a two parameter function into parser world *)
let lift2 f p1 p2 =
  applyP (p1 |> applyP (wrap f)) p2
(** Modify the result of a parser from 'a to 'b *)
let map (f: 'a -> 'b) (p: 'a parser) : 'b parser = {
  run = fun text ->
    match p.run text with
    | text', Ok x -> text', Ok (f x)
    | text', Error err -> text', Error err
}
(** Parse until the predicate p returns false. p must be false at some point, if end of input is reached, error *)
let parse_until (p : char -> bool) : string parser = {
  run = fun text ->
    let n = String.length text.data in
    let i = ref 0 in
    try
    while String.get text.data !i |> p do
      incr i
    done;
      let text' = splice !i (n - !i) text in
      text', Ok(String.sub text.data 0 !i)
    with
    | Invalid_argument _ -> splice !i (n - !i) text, Error "End of string reached, but expected more."
}
(** Parse while the predicate p returns true. If p never returns false, we parse the whole input *)
let parse_while (p : char -> bool) : string parser = {
  run = fun text ->
    let n = String.length text.data in
    let i = ref 0 in
    while !i < n && String.get text.data !i |> p do
      incr i
    done;
    let text' = splice !i (n - !i) text in
    text', Ok(String.sub text.data 0 !i)
}

(** Expect a particular keyword to occur *)
let expect (pref : string) : string parser = {
  run = fun text ->
    let unexpected_token_error = Printf.sprintf "expected `%s`" pref
    in try
      let pref_n = String.length pref in
      let text_n = String.length text.data in
      let pref_in = text |> splice 0 pref_n in
      if String.equal pref_in.data pref then
        let rest = text |> splice pref_n (text_n - pref_n) in
        rest, Ok pref
      else
        text, Error unexpected_token_error
    with
     Invalid_argument _ -> text, Error unexpected_token_error
}
(** Expects without pref, but fails if full can be parsed *)
let expect_prefix (pref : string) (full : string) : string parser = {
  run = fun text ->
    let unexpected_token_error = Printf.sprintf "expected `%s`" pref in
    match (expect full).run text with
    | text', Ok _ -> text', Error unexpected_token_error
    | _, Error _ -> (expect pref).run text
}
(** Combine the results of two parsers. Fails if any of the given parsers fail *)
let ( <*> ) (p1 : 'a parser) (p2 : 'b parser) : ('a * 'b) parser =
  p1 |> bind (fun r1 -> p2 |> bind (fun r2 -> wrap (r1, r2)))
(** Use the result of the right-handside parser *)
let ( *> ) (p1 : 'a parser) (p2 : 'b parser) : 'b parser =
  p1 |> bind (fun _ -> p2 |> bind (fun r2 -> wrap r2))
(** Use the result of the left-handside parser *)
let ( <* ) (p1 : 'a parser) (p2 : 'b parser) : 'a parser =
  p1 |> bind (fun r1 -> p2 |> bind (fun _ -> wrap r1))

(** Use the result of either parser. Fails only if both fail *)
let ( <|> ) (p1 : 'a parser) (p2 : 'a parser) : 'a parser = {
  run = fun text ->
    let text', res = p1.run text in
    match res with
    | Ok x -> text', Ok x
    | Error _ -> p2.run text
}
(** Choose any out of a list of parsers. For a list [p1;p2;...;pN] the parser p1 is run, then p2, and so on. *)
let choice (ps : 'a parser list) : 'a parser = Util.list_reduce (<|>) ps
(** Does not fail, but returns an optional upon failure *)
let opt (p : 'a parser) : 'a option parser =
  let some = p |> map (fun x -> Some x) in
  let none = wrap None in
  some <|> none
(** Parse exactly n times *)
let many_exact (n : int) (p : 'a parser) : 'a list parser = {
  run =
    let rec loop i xs text' =
      if i < n then
        let text'', res = p.run text' in
        match res with
        | Ok x -> loop (i + 1) (x :: xs) text''
        | Error e -> text'', Error e
      else
        text', Ok (List.rev xs)
    in loop 0 []
}
(** Never fails. Basically, kleene star *)
let many (p : 'a parser) : 'a list parser = {
  run = fun text ->
    let xs = ref [] in
    let rec loop text' =
      let text'', res = p.run text' in
      match res with
      | Ok x -> xs := x :: !xs; loop text''
      | Error _ -> text'
    in
    let oof = loop text in
    oof, Ok (List.rev !xs)
}
(** Basically `pp*` as regex, where `p` is the given parser. *)
let many1 (p : 'a parser) : 'a list parser =
  p |> bind (fun hd -> many p |> bind (fun tl -> wrap (hd :: tl)))
(** Read exactly one character of the string. Useful after parse_while *)
let arbitrary_char: char parser = {
  run = fun text ->
    let n = String.length text.data in
    try
      splice 1 (n - 1) text, Ok (String.get text.data 0)
    with
      Invalid_argument _ -> text, Error "expected any character"
}
(** Parses any of the given characters. *)
let any (set : char list) : char parser = {
  run = fun text ->
    assert(List.length set > 0);
    let error = Printf.sprintf "One of the characters [%s] expected!" (set |> List.to_seq |> String.of_seq) in
    try
      let head = String.get text.data 0 in
      match List.find_opt (fun x -> x = head) set with
      | Some _ -> (splice 1 (String.length text.data - 1) text), Ok head
      | None -> text, Error error
    with
      Invalid_argument _ -> text, Error error
}
(** Parses any of the given strings. *)
let anys (set : string list) : string parser =
    assert(List.length set > 0);
    let error = Printf.sprintf "One of [%s] expected!" (set |> Util.string_of_sep_list (fun x -> x) ", ") in
    info error (choice (set |> List.map (fun a -> expect a)))

(** Convenience function accepting a string constituting a set of characters that should be matched against *)
let any_char (str : string) : char parser =
    any (str |> String.to_seq |> List.of_seq)
(** Applies a list of parsers and accumulates the results in a list *)
let sequence (p : 'a parser list) : 'a list parser = {
  run = fun text ->
    let rec loop text' (ps : 'a parser list) =
      match ps with
      | [] -> text', Ok []
      | p' :: ps' ->
        text' |> (map (fun (a,b) -> a :: b) (p' <*> ({ run = fun t -> loop t ps' }))).run
    in loop text p
}
(** Parses a non-empty list in the input seperated by sep. Individual elements of the list are parsed by p *)
let sep_by_1 (p : 'a parser) (sep : 'b parser) : 'a list parser =
  let sep_then_p = sep *> p in
  p <*> many sep_then_p |> map (fun (x,list) -> x :: list)
(** Parses a potentially empty list seperated by sep. Individual elements of the list are parsed by p *)
let sep_by (p : 'a parser ) (sep : 'b parser) : 'a list parser =
  sep_by_1 p sep <|> wrap []

(** Ignore any whitespace characters *)
let ws = parse_while (fun x -> x == ' ' || x == '\r' || x == ' ' || x == '\n')

let is_digit x = ('0' <= x && x <= '9')
let digits = parse_while is_digit
let number = {
  run = fun text ->
    let text', res = digits.run text in
    match res with
    | Ok x ->
      begin
        if String.length x = 0 then
          text', Error "Number expected."
        else if String.length x > 1 then
          let _, res' = x |> mk_text |> (expect "0").run in
          match res' with
          | Ok _ -> text', Error "Numbers must not start with 0."
          | Error _ -> text', Ok x
        else
          text', Ok x
      end
    | Error s -> text', Error s
}
(** Simply run a parser on a given input *)
let run (p : 'a parser) (s : string) : ('a, error) result =
  match p.run (mk_text s) with
  | _, Ok x -> Ok x
  | text', Error desc -> Error { info = desc; pos = text'.pos }
