open Ast
open Ast.Kraken

exception Not_a_value
exception Type_error

(** evaluates a binary operation *)
let eval_op (op':op) (v1:Kraken.exp) (v2:Kraken.exp) : Kraken.exp =
  match !v1,op',!v2 with
  | Int_e i, Plus, Int_e j -> int_e (i+j)
  | Int_e i, Minus, Int_e j -> int_e (i-j)
  | Int_e i, Mult, Int_e j -> int_e (i*j)
  | Int_e i, Div, Int_e j -> int_e (i/j)
  | Int_e i, Le, Int_e j -> int_e (if i<j then 1 else 0)
  | Int_e i, LeEq, Int_e j -> int_e (if i<=j then 1 else 0)
  | Int_e i, Eq, Int_e j -> int_e (if i=j then 1 else 0)
  | Int_e i, Ne, Int_e j -> int_e (if i<>j then 1 else 0)
  | Int_e i, GtEq, Int_e j -> int_e (if i>=j then 1 else 0)
  | Int_e i, Gt, Int_e j -> int_e (if i>j then 1 else 0)
  | _,_,_ -> if is_value v1 && is_value v2 then raise Type_error else raise Not_a_value


(** evaluates an arbitrary expression *)
let rec eval_e (e : expr) =
  match e with
  | Op_e (e1,o,e2) -> eval_op o (eval_e !e1) (eval_e !e2)
  | Let_e (x,e1,e2) -> eval_e !(substitute (eval_e !e1) (string_of_var_e x) e2)
  | Lam_e _ -> mk_exp e
  | App_e (e1,e2) ->
    let f = eval_e !e1 in
    (match !f with
    | Lam_e (x,e') -> (* call by value *) eval_e !(substitute (eval_e !e2) (string_of_var_e x) e')
    | _ -> raise Type_error)
  | _ -> mk_exp_rehashing (Kraken.map (fun (e' : exp) -> eval_e !e') e)

(** evaluates a statement *)
let eval_s (s : Kraken.stmt) =
  match s with
  | Kraken.Expr_s e -> Kraken.Expr_s (eval_e !e)

(** evaluates a whole program *)
let run (p:Kraken.program) = List.fold_left (fun init x -> List.concat [init ; [eval_s x]]) [] p
