open Ast


type value = Type_v
           | Int_v of int
           | Lam_v of value * value

type ctxelem = { name : Kraken.var ; t : Kraken.expr }
type gamma = ctxelem list

let bind f x = Result.bind x f

exception Not_a_value
let must_be_a_value (env:gamma) (e:Kraken.expr) = match e with
  | (Kraken.Type_e | Kraken.Int_e _) -> e
  | Kraken.Lam_e _ -> e
  | Kraken.Var_e (s,_) -> begin
      match env |> List.find_opt (fun (a:ctxelem) -> a.name = s) with
      | Some _ -> e (* context only stores values *)
      | None -> raise Not_a_value
    end
  | _ -> raise Not_a_value

(** looksup e in env. expects e to be a var *)
let lookup env s =
  match env |> List.find_opt (fun a -> a.name = s) with
  | Some c -> c.t
  | None -> raise Not_a_value

(* Throws a `Not_a_value` exception if e does not eval to a value *)
let zap_to_value (env:gamma) (e:Kraken.expr) =
  let v = Eval.eval_e e in must_be_a_value env !v

let rec are_types_equal env a b =
  match a,b with
  | Kraken.Var_e(x,_), _ -> are_types_equal env (lookup env x) b
  | _, Kraken.Var_e(x,_) -> are_types_equal env a (lookup env x)
  | _, _ -> a = b (* TODO: unification :) *)

let rec check (env:gamma) (e:Kraken.expr) (tau:Kraken.expr) =
  try
    match e,tau with
    | Kraken.Lam_e(x,e),Kraken.Lam_e(y,b) -> begin
        match y.t with
        | Some a -> check ({ name = string_of_var_e x ; t = zap_to_value env a } :: env) !e !b
        | None -> Error "Codomain does not seem to be annotated appropriately."
      end
    | _,_ -> synth env e |> bind (fun a ->
        if are_types_equal env a tau then
          Ok ()
        else
          Error (Printf.sprintf "Type mismatch, expected %s but got %s." (string_of_exp e) (string_of_exp tau))
      )
  with
  | Not_a_value -> Error "check: Value required."

and synth (env:gamma) (e:Kraken.expr) =
  try
    match e with
    | Kraken.Type_e -> Ok Kraken.Type_e
    | Kraken.Var_e (x,ot) -> begin
        match ot with
        | None -> begin (*var rule*)
            match env |> List.find_opt (fun (a:ctxelem) -> a.name = x) with
            | Some o -> Ok o.t
            | None -> Error "Variable with unknown type."
          end
        | Some t -> begin (*annot rule*)
            check env !t (Kraken.Type_e) |> bind (fun _ -> let d = !t in
                                                  check env e d |> bind (fun _ -> Ok d))
          end
      end
    | Kraken.App_e(e1,e2) -> begin
        synth env !e1 |> bind (fun fn ->
          match fn with
          | Kraken.Lam_e(x,b) -> begin
              match (ty_opt_of_var_e x) with
              | Some a -> check env !e2 !a |> bind (fun _ -> Ok (zap_to_value env !(substitute e2 (string_of_var_e x) b)))
              | None -> Error "Unknown codomain."
            end
          | _ -> Error (Printf.sprintf "Expected a function type, but inferred %s." (string_of_exp fn))
          )
      end
    | Kraken.Lam_e(x,b) -> begin
        match (ty_opt_of_var_e x) with
        | Some annot ->
          (check env !annot !(Kraken.type_e)) |> bind (fun _ ->
                (synth ({ name = string_of_var_e x ; t = (*zap_to_value env*) !annot } :: env) !b))
        | None -> Error "Unknown codomain, must be annotated."
      end
    | Kraken.Let_e(x,e1,e2) -> begin
        synth env !e1 |> bind (fun t ->
        match (ty_opt_of_var_e x) with
        | Some annot -> begin
            if are_types_equal env !annot t then
              synth ({ name = string_of_var_e x ; t = zap_to_value env !annot } :: env) !e2
            else
              Error (Printf.sprintf "Inferred type (%s) is incompatible with annotation (%s)."
                       (string_of_exp t) (string_of_exp !annot))
          end
        | None -> synth ({ name = string_of_var_e x ; t = zap_to_value env t } :: env) !e2
        )
      end
    | _ -> Error "Unknown type-synthesis."
  with
  | Not_a_value -> Error "synth: Value required."


let typecheck_stmt = function
  | Kraken.Expr_s e -> synth [] !e

let typecheck prog = List.map typecheck_stmt prog
