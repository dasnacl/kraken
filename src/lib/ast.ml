
module Kraken = struct
  type var = string [@@deriving eq, show]
  module VarSet = Set.Make(String)

  let gensym = let counter = ref 0 in
                fun () ->
                  incr counter; "$x" ^ string_of_int !counter
  type uid = int
  let combine_uid h1 h2 = Int.logxor h1 (h2 + (int_of_string "0x9e3779b9")
                                            + (Int.shift_right h2 6) + (Int.shift_left h1 2))
  type op = Plus | Minus | Mult | Div
          | Le   | LeEq  | Eq  | Ne  | Gt | GtEq
          [@@deriving eq, show, enum]

  type 'a exp_dat =
    | Type_e
    | Int_e of int
    | Var_e of var * ('a option) (* name, type annotation *)
    | Op_e of 'a * op * 'a
    | Let_e of 'a * 'a * 'a
    | Lam_e of 'a * 'a (* Var_e, body *)
    | App_e of 'a * 'a
    [@@deriving map, eq]
  type exp = { e : exp exp_dat ;
               hash : uid ;
               t : (exp exp_dat) option ;
             }
  type expr = exp exp_dat

  let general_exp_uid hasher (e : expr) : uid =
    match e with
    | Type_e -> 1
    | Int_e n -> 2 * n
    | Var_e(x,ot) -> begin
        let h1 = 3 + (17 + Hashtbl.hash x) in
        match ot with
      | Some t -> combine_uid h1 (hasher t)
      | None -> h1
      end
    | Op_e (e1,o,e2) ->
      let ho = op_to_enum o in
      let h1 = hasher e1 in
      let h2 = hasher e2 in
      combine_uid ho (combine_uid h1 h2) + 17
    | Let_e (x,e1,e2) ->
      let ho = Hashtbl.hash x in
      let h1 = hasher e1 in
      let h2 = hasher e2 in
      combine_uid ho (combine_uid h1 h2) + 43
    | Lam_e (x,e) ->
      let h1 = Hashtbl.hash x in
      let h2 = hasher e in
      combine_uid h1 h2 + 31
    | App_e (e1,e2) ->
      combine_uid e1.hash e2.hash + 73
  let exp_uid = general_exp_uid (fun e' -> e'.hash)
  let rec rec_exp_uid e = general_exp_uid (rec_exp_uid) e.e
  let rec rec_exp_dat_uid e = general_exp_uid (fun e' -> rec_exp_dat_uid e'.e) e

  module ExpH = struct
    type t = exp
    type key = int
    let equal = fun (e1 : exp) (e2 : exp) -> equal_exp_dat (fun (a : exp) (b : exp) -> a = b) e1.e e2.e
    let hash n = n.hash
  end
  let exp_table : (int, exp) Hashtbl.t = (Hashtbl.create 1493)

  let mk_exp_core h n =
    let o = Hashtbl.find_opt exp_table h in
    match o with
    | None ->
      let n' = { e = n ; hash = h ; t = None } in
      Hashtbl.add exp_table h n' ; n' (* create node *)
    | Some e -> e

  let mk_exp (n : expr) = mk_exp_core (exp_uid n) n
  let mk_exp_rehashing (n : expr) = mk_exp_core (rec_exp_dat_uid n) n

  (* Smart Constructors *)
  let type_e = mk_exp (Type_e)
  let int_e i = mk_exp (Int_e i)
  let var_e v = mk_exp (Var_e (v, None))
  let var_t v t = mk_exp (Var_e (v, Some t))
  let op_e e1 o e2 = mk_exp (Op_e(e1,o,e2))
  let let_e x e1 e2 = mk_exp (Let_e(x,e1,e2))
  let lam_e x e = mk_exp (Lam_e(x,e))
  let arrow_e t1 t2 = mk_exp (Lam_e(var_t "_" t1, t2))
  let app_e e1 e2 = mk_exp (App_e(e1,e2))

  type stmt =
    | Expr_s of exp
  type program = stmt list

  let map = map_exp_dat
  let map_rehashing f (e : exp) = mk_exp_rehashing (map f e.e)

  let fold f join init e =
    let foo a = join init (f a)
    in match e with
    | (Type_e | Int_e _ | Var_e _) -> init
    | Op_e (e1,_,e2) -> join (foo e1) (foo e2)
    | Let_e (_,e1,e2) -> join (foo e1) (foo e2)
    | Lam_e (_,e') -> foo e'
    | App_e (e1, e2) -> join (foo e1) (foo e2)
end
let (!) (e:Kraken.exp) = e.e
let string_of_var_e (e:Kraken.exp) = match !e with
  | Kraken.Var_e(s,_) -> s
  | _ -> raise (Invalid_argument "Expected a variable")
let ty_opt_of_var_e (e:Kraken.exp) = match !e with
  | Kraken.Var_e(_,t) -> t
  | _ -> raise (Invalid_argument "Expected a variable")

let string_of_op (o:Kraken.op) : string =
  match o with
  | Kraken.Plus -> "+"
  | Kraken.Minus -> "-"
  | Kraken.Mult -> "*"
  | Kraken.Div -> "/"
  | Kraken.Le -> "<"
  | Kraken.LeEq -> "<="
  | Kraken.Eq -> "="
  | Kraken.Ne -> "!="
  | Kraken.Gt -> ">"
  | Kraken.GtEq -> ">="

let is_value (e:Kraken.exp) : bool =
  match !e with
  | (Kraken.Type_e | Kraken.Int_e _ | Kraken.Var_e _) -> true
  | Kraken.Lam_e _ -> true
  | _ -> true

(** [fv e] is a set-like list of the free variables of [e]. *)
let rec fv (e:Kraken.exp) : Kraken.VarSet.t =
  match !e with
  | Kraken.Var_e(x,_) -> Kraken.VarSet.singleton x
  | Kraken.Lam_e(x,e') -> Kraken.VarSet.diff (fv e') (Kraken.VarSet.singleton (string_of_var_e x))
  | Kraken.Let_e(z,e1,e2) -> Kraken.VarSet.union (fv e1) (Kraken.VarSet.diff (fv e2) (Kraken.VarSet.singleton (string_of_var_e z)))
  | _ -> Kraken.fold fv Kraken.VarSet.union Kraken.VarSet.empty !e

(** reames x to y in expression e *)
let rec rename (x:Kraken.var) (y:Kraken.var) (e:Kraken.exp) : Kraken.exp =
  match !e with
  | Kraken.Var_e(str,_) -> if str = x then e else Kraken.var_e y
  | Kraken.Let_e(z,e1,e2) -> Kraken.let_e z (rename x y e1) (if (string_of_var_e z) = x then e2 else rename x y e2)
  | Kraken.Lam_e(z,e) -> Kraken.lam_e z (if (string_of_var_e z) = x then e else rename x y e)
  | _ -> Kraken.mk_exp_rehashing (Kraken.map (rename x y) e.e)

(** substitutes v for x in e *)
let substitute (v:Kraken.exp) (x:Kraken.var) (e:Kraken.exp) : Kraken.exp =
  let rec subst (e:Kraken.exp) : Kraken.exp =
    match !e with
    | Kraken.Var_e(y,_) -> if x = y then v else e
    | Kraken.Let_e(y,e1,e2) -> Kraken.let_e y (subst e1) (if x = (string_of_var_e y) then e2 else subst e2)
    | Kraken.Lam_e(y,e') ->
      let ys = string_of_var_e y in
      if x = ys then
        Kraken.lam_e y e'
      else if not (Kraken.VarSet.mem ys (fv v)) then
        Kraken.lam_e y (subst e')
      else
        let fresh = Kraken.gensym() in
        let new_body = rename ys fresh e' in
        let unwrap o = match o with
          | Some x -> Kraken.var_t fresh x
          | None -> Kraken.var_e fresh
        in
        Kraken.lam_e (unwrap (ty_opt_of_var_e y)) (subst new_body)
    | _ -> Kraken.map_rehashing subst e
  in
    subst e

(** prints an expression *)
let rec string_of_exp (e:Kraken.expr) : string =
  match e with
  | Kraken.Type_e -> Printf.sprintf "Type"
  | Kraken.Int_e i -> Printf.sprintf "%d" i
  | Kraken.Var_e(x,_) -> Printf.sprintf "%s" x
  | Kraken.Op_e(e1,op,e2) ->
    (Printf.sprintf "(") ^
    (string_of_exp !e1) ^
    (Printf.sprintf " %s " (string_of_op op)) ^
    (string_of_exp !e2) ^
    (Printf.sprintf ")")
  | Kraken.Let_e(x,e1,e2) ->
    (Printf.sprintf "(let %s = " (string_of_var_e x)) ^
    (string_of_exp !e1) ^
    (Printf.sprintf "; ") ^
    (string_of_exp !e2) ^
    (Printf.sprintf ")")
  | Kraken.App_e(e1,e2) ->
    (Printf.sprintf "(") ^
    (string_of_exp !e1) ^
    (Printf.sprintf " ") ^
    (string_of_exp !e2) ^
    (Printf.sprintf ")")
  | Kraken.Lam_e(x,e) ->
    (Printf.sprintf "(\\%s. " (string_of_var_e x)) ^
    (string_of_exp !e) ^
    (Printf.sprintf ")")

let print_exp e = Printf.printf "%s" (string_of_exp !e)

(** prints a statement *)
let print_stmt (s:Kraken.stmt) : unit =
  match s with
  | Kraken.Expr_s e ->
    print_exp e;
    Printf.printf ";"
